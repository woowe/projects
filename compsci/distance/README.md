# Distance

Command-line tool that uses the Google Distance Matrix API to informs the user
 of the distance between entered locations and how long it takes to travel the
 distance with the selected transportation method.

Used to practice working with a RESTful API in Python.

To use, will need a Distance Matrix API key with billing enabled and a small
 Python module named personal_key.py that returns your API key.
 - For API key and billing, see:
    https://developers.google.com/maps/documentation/distance-matrix/intro#before-you-begin
 - For Python module, replace <YOUR_API_KEY> with *your* Distance Matrix API key
    in the code below and save the file as personal_key.py in the same directory
    as your distance.py file.

    ```
    def get_key():
        return "<YOUR_API_KEY>"
    ```
