const myTimer = require('./modules/timer.js');

function alterTimer(event) {
  if (event.srcElement.id == 'start') {
    myTimer.startTimer();
  } else if (event.srcElement.id == 'stop') {
    myTimer.stopTimer();
  }
}

function initialize() {
  return myTimer.getTimer();
}

document.addEventListener('DOMContentLoaded', function() {
  document.getElementById('start').addEventListener('click', alterTimer);
  document.getElementById('stop').addEventListener('click', alterTimer);
})