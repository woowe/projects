var timerSeconds = 1200;
var running = false;

function runTimer() {
  if (running) {
    if (timerSeconds >= 0) {
      if (timerSeconds < 600) {
        var minutes = parseInt(timerSeconds / 60);
        var seconds = timerSeconds % 60;
        var timerOutput = document.getElementById('timer');

        if (seconds < 10) {
          seconds = '0' + seconds;
        }

        if (minutes > 0) {
          timerOutput.innerHTML = minutes + ':' + seconds;
        } else {
          timerOutput.innerHTML = ':' + seconds;
        }
      } else {
        var minutes = parseInt(timerSeconds / 60);
        var seconds = timerSeconds % 60;
        var timerOutput = document.getElementById('timer');

        if (seconds < 10) {
          seconds = '0' + seconds;
        }

        timerOutput.firstChild.nodeValue = minutes + ':' + seconds;
      }
    }

    timerSeconds--;
    window.setTimeout(function(){runTimer();}, 1000);
  }

  if (timerSeconds < 0) {
    running = false;
    var timerOutput = document.getElementById('timer');

    timerOutput.firstChild.nodeValue = 'Rest your eyes!';
    document.getElementById('start').firstChild.nodeValue = 'Restart';
    document.getElementById('stop').firstChild.nodeValue = 'Reset';
  }
}

module.exports = {
  startTimer: function() {
    if (document.getElementById('start').firstChild.nodeValue == 'Resume') {
      document.getElementById('start').firstChild.nodeValue = 'Start';
      document.getElementById('stop').firstChild.nodeValue = 'Stop';
    }

    if (document.getElementById('start').firstChild.nodeValue == 'Restart') {
      restartTimer();
    } else {
      if (!running) {
        running = true;
        runTimer();
      }
    }
  },
  stopTimer: function() {
    if (timerSeconds < 1200) {
      if (document.getElementById('stop').firstChild.nodeValue == 'Reset') {
        resetTimer();
      } else {
        running = false;
        var stopButton = document.getElementById('stop');
        var startButton = document.getElementById('start');

        stopButton.firstChild.nodeValue = 'Reset';
        startButton.firstChild.nodeValue = 'Resume';
      }
    }
  },
  getTimer: function() {
    var displayedTimer = document.getElementById('timer');
    var minutes = parseInt(timerSeconds / 60);
    var seconds = timerSeconds % 60;

    displayedTimer.innerHTML = minutes + ':' + seconds + '0';
  }
}

function resetTimer() {
  running = false;
  timerSeconds = 1200;

  document.getElementById('start').firstChild.nodeValue = 'Start';
  document.getElementById('stop').firstChild.nodeValue = 'Stop';

  var resetMinutes = parseInt(timerSeconds / 60);
  var resetSeconds = timerSeconds % 60;

  var timerResetOutput = document.getElementById('timer');
  timerResetOutput.innerHTML = resetMinutes + ':' + resetSeconds + '0';
}

function restartTimer() {
  document.getElementById('start').firstChild.nodeValue = 'Start';
  document.getElementById('stop').firstChild.nodeValue = 'Stop';
  timerSeconds = 1200;
  running = true;

  runTimer();
}