/**
 * main.js used to create windows and handle all the system events the app may
 *  encounter
 * 
 * reference: electronjs.org
 */

const { app, BrowserWindow } = require('electron');

let win;
const gotTheLock = app.requestSingleInstanceLock();

function createWindow() {
  // Create the browser window
  win = new BrowserWindow({
    height: 200,
    width: 300,
    webPreferences: {
      nodeIntegration: true
      // devTools: false
    }
  });

  // load the index.html of the app
  win.loadFile('index.html');

  // emitted when the window is closed
  win.on('closed', () => {
    // dereference the window object
    // if app supports multi windows, this is where you would delete the array
    //  that stores those windows
    win = null;
  })
}

if (!gotTheLock) {
  app.quit();
} else {
  app.on('second-instance', (event, commandLine, workingDirectory) => {
    if (win) {
      if (win.isMinimized()) {
        win.restore()
        win.focus();
      }
    }
  });

  app.on('ready', () => {
    // this method will be called when Electron has finished initialization and is
    // ready to create browser windows. some APIs can only be used after this event
    createWindow();
  });

  // quit when all windows are closed
  app.on('window-all-closed', () => {
    // macOS application and menu bar stays active until user quits explicitly
    //  with Cmd + Q
    if (process.platform !== 'darwin') {
      app.quit();
    }
  });

  app.on('activate', () => {
    if (process.platform === 'darwin' && win === null) {
      createWindow();
    }
  });
}